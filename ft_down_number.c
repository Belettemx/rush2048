/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_down_number.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 13:50:15 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/01 16:14:19 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static int	ft_move_down(t_env *env, int x, int y, int *move)
{
	while (y < 3)
	{
		x = 0;
		while (x <= 3)
		{
			if (env->squares[y][x] && !env->squares[y + 1][x])
			{
				env->squares[y + 1][x] = env->squares[y][x];
				env->squares[y][x] = 0;
				*move = 1;
				return (1);
			}
			x++;
		}
		y++;
	}
	return (0);
}

static void	ft_add_down_two(t_env *env, int *move, int x, int y)
{
	if (env->squares[y][x] == env->squares[y - 1][x] && env->squares[y][x])
	{
		env->squares[y][x] *= 2;
		env->squares[y - 1][x] = 0;
		if (env->squares[y - 2][x] == env->squares[y - 3][x])
		{
			env->squares[y - 2][x] *= 2;
			env->squares[y - 3][x] = 0;
		}
		*move = 1;
	}
	else if (env->squares[y - 1][x] == env->squares[y - 2][x]\
			&& env->squares[y - 1][x])
	{
		env->squares[y - 1][x] *= 2;
		env->squares[y - 2][x] = 0;
		*move = 1;
	}
	else if (env->squares[y - 2][x] == env->squares[y - 3][x]\
			&& env->squares[y - 2][x])
	{
		env->squares[y - 2][x] *= 2;
		env->squares[y - 3][x] = 0;
		*move = 1;
	}
}

static void	ft_add_down(t_env *env, int *move)
{
	int		y;
	int		x;

	x = 0;
	while (x < 4)
	{
		y = 3;
		ft_add_down_two(env, move, x, y);
		x++;
	}
}

int			ft_down(t_env *env)
{
	int move;

	move = 0;
	while (ft_move_down(env, 0, 0, &move))
		;
	ft_add_down(env, &move);
	while (ft_move_down(env, 0, 0, &move))
		;
	if (move == 1)
		env->mv_nbr++;
	return (move);
}
