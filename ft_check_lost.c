/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_lost.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 10:58:21 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/01 14:10:03 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

int ft_check_lost(t_env *env)
{
	int x;
	int y;
	int i;

	x = 0;
	i = 0;
	while (x < 4)
	{
		y = 0;
		while (y < 4)
		{
			if (env->squares[y][x] != 0)
				i++;
			y++;
		}
		x++;
	}
	if (i == 16)
	{
		return (i = ft_move_poss(env));
	}
	return (1);
}
