NAME = game_2048

CC = gcc -g3

SOURCE = ft_check_error.c ft_draw.c ft_rand.c game2048.c put_color.c \
ft_check_lost.c ft_left_number.c ft_right_number.c check_number_color.c \
ft_check_win.c ft_move_poss.c ft_up_number.c print_number.c \
ft_algorithme.c ft_down_number.c ft_put_cadre.c print_number_cases.c

NOM = $(basename $(SOURCE))

OBJET = $(addsuffix .o, $(NOM))

FLAGS = -Wall -Werror -Wextra

LIB = -lncurses -L libft/ -lft -I.

all: $(NAME)

$(NAME): libft $(OBJET)
		@make -C libft/
		@gcc -o $@ $(LIB) $(OBJET) $(FLAGS)

%.o: %.c
		@$(CC) -o $@ -c $< $(FLAGS)

clean:
		@make -C libft/ clean
		@/bin/rm -f $(OBJET)

fclean: clean
		@make -C libft/ fclean
		@/bin/rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re
