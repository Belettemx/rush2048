/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_right_number.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 16:04:50 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/01 16:08:56 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static int	ft_move_right(t_env *env, int x, int y, int *move)
{
	while (y <= 3)
	{
		x = 3;
		while (x > 0)
		{
			if (env->squares[y][x - 1] && !env->squares[y][x])
			{
				env->squares[y][x] = env->squares[y][x - 1];
				env->squares[y][x - 1] = 0;
				*move = 1;
				return (1);
			}
			x--;
		}
		y++;
	}
	return (0);
}

static void	ft_add_right_two(t_env *env, int *move, int x, int y)
{
	if (env->squares[y][x] == env->squares[y][x - 1] && env->squares[y][x])
	{
		env->squares[y][x] *= 2;
		env->squares[y][x - 1] = 0;
		if (env->squares[y][x - 2] == env->squares[y][x - 3])
		{
			env->squares[y][x - 2] *= 2;
			env->squares[y][x - 3] = 0;
		}
		*move = 1;
	}
	else if (env->squares[y][x - 1] == env->squares[y][x - 2]\
			&& env->squares[y][x - 1])
	{
		env->squares[y][x - 1] *= 2;
		env->squares[y][x - 2] = 0;
		*move = 1;
	}
	else if (env->squares[y][x - 2] == env->squares[y][x - 3]\
			&& env->squares[y][x - 2])
	{
		env->squares[y][x - 2] *= 2;
		env->squares[y][x - 3] = 0;
		*move = 1;
	}
}

static void	ft_add_right(t_env *env, int *move)
{
	int		y;
	int		x;

	y = 0;
	while (y < 4)
	{
		x = 3;
		ft_add_right_two(env, move, x, y);
		y++;
	}
}

int			ft_right(t_env *env)
{
	int		move;

	move = 0;
	while (ft_move_right(env, 3, 0, &move))
		;
	ft_add_right(env, &move);
	while (ft_move_right(env, 3, 0, &move))
		;
	if (move == 1)
		env->mv_nbr++;
	return (move);
}
