/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 00:48:37 by jpiazzal          #+#    #+#             */
/*   Updated: 2015/03/01 21:17:01 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAME_H
# define GAME_H

# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <string.h>
# include <ncurses.h>
# include <time.h>
# include "libft/libft.h"

# define WIN_VALUE 16
# define INIT 300

enum			e_const
{
	WIN_1 = 2048, WIN_2 = 65536,
};

typedef struct	s_env
{
	int			squares[4][4];
	int			win_0;
	int			win_1;
	int			lost;
	int			x;
	int			y;
	int			randx;
	int			randy;
	int			mv_nbr;
}				t_env;

void			put_color(int x, int y, int c, char *str);
int				ft_error(char *str);
int				ft_up(t_env *env);
int				ft_down(t_env *env);
int				ft_left(t_env *env);
int				ft_right(t_env *env);
void			ft_rand_number(t_env *env);
int				ft_algorithme(int key, t_env *env);
void			print_number(int row, int col, t_env *env);
void			draw_cases(int row, int col, t_env *env);
void			initialisation_color(void);
int				check_window_size(int col, int row, int key);
void			print_game(void);
int				check_power_two(void);
int				ft_nbrlen(int n);
int				ft_check_win(t_env *env);
int				ft_check_lost(t_env *env);
int				ft_move_poss(t_env *env);
void			ft_rand(t_env *env);
void			ft_put_cadre(int x, int y, int c, char *str);
int				check_number_color(int nb);
void			put_color(int x, int y, int c, char *str);
void			print_number_cases(t_env *env, int row, int col);
void			print_number(int row, int col, t_env *env);

#endif
