/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 00:35:44 by jpiazzal          #+#    #+#             */
/*   Updated: 2015/03/01 00:35:45 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static void	print_background(int row, int col)
{
	int		x;
	int		y;

	y = -1;
	while (++y < row)
	{
		x = -1;
		while (++x < col)
			put_color(x, y, 53, " ");
	}
}

static void	print_best_case(int col, t_env *env)
{
	int		x;
	int		y;
	int		result;
	char	*str;

	y = -1;
	result = 0;
	while (++y <= 3)
	{
		x = -1;
		while (++x <= 3)
			if (env->squares[y][x] > result)
				result = env->squares[y][x];
	}
	put_color(col / 3 * 2 - 6, 0, 50, "BEST CASE : ");
	str = ft_itoa(result);
	put_color(col / 3 * 2 + 6, 0, 50, str);
	free(str);
}

static void	print_score(int col, t_env *env)
{
	int		x;
	int		y;
	int		result;
	char	*str;

	y = -1;
	result = 0;
	while (++y <= 3)
	{
		x = -1;
		while (++x <= 3)
			result += env->squares[y][x];
	}
	put_color(col / 3 - 4, 0, 50, "SCORE : ");
	str = ft_itoa(result);
	put_color(col / 3 + 4, 0, 50, str);
	free(str);
}

static void	print_mv_number(int row, int col, t_env *env)
{
	char	*str;

	put_color(col / 2 - 6, row - 1, 50, "MOVEMENTS : ");
	str = ft_itoa(env->mv_nbr);
	put_color(col / 2 + 6, row - 1, 50, str);
	free(str);
}

void		draw_cases(int row, int col, t_env *env)
{
	while ((row - 1) % 4 != 0)
		row--;
	while ((col - 1) % 4 != 0)
		col--;
	env->y = -1;
	print_background(row, col);
	print_number_cases(env, row, col);
	while (++env->y < row)
	{
		env->x = -1;
		while (++env->x < col)
		{
			print_number(row, col, env);
			if (env->x == 0 || env->x == col / 4 || env->x == col / 2
				|| env->x == col / 4 * 3 || env->x == (col - 1))
				put_color(env->x, env->y, 51, " ");
			if (env->y == 0 || env->y == row / 4 || env->y == row / 2
				|| env->y == row / 4 * 3 || env->y == (row - 1))
				put_color(env->x, env->y, 51, " ");
		}
	}
	print_score(col, env);
	print_best_case(col, env);
	print_mv_number(row, col, env);
}
