/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_number_cases.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 21:11:16 by jpiazzal          #+#    #+#             */
/*   Updated: 2015/03/01 21:11:17 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static void	print_cases(int tx, int ty, int c)
{
	if (c == 0)
		put_color(tx, ty, 53, " ");
	else
		put_color(tx, ty, c, " ");
}

void		print_number_cases(t_env *env, int row, int col)
{
	int		x;
	int		y;
	int		tx;
	int		ty;
	int		c;

	y = -1;
	while (++y <= 3)
	{
		x = -1;
		while (++x <= 3)
		{
			c = check_number_color(env->squares[y][x]);
			ty = row / 4 * y;
			while (++ty < row / 4 * (y + 1))
			{
				tx = col / 4 * x;
				while (++tx < col / 4 * (x + 1))
					print_cases(tx, ty, c);
			}
		}
	}
}
