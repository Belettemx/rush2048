/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_number_color.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 21:11:52 by jpiazzal          #+#    #+#             */
/*   Updated: 2015/03/01 21:11:55 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

int		check_number_color(int nb)
{
	int		tmp;
	int		i;

	tmp = 2;
	i = 1;
	while (tmp != 4096)
	{
		if (tmp == nb)
			return (i);
		tmp *= 2;
		i++;
	}
	return (0);
}
