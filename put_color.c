/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 21:11:28 by jpiazzal          #+#    #+#             */
/*   Updated: 2015/03/01 21:11:29 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

void	put_color(int x, int y, int c, char *str)
{
	if (c == 54 || c == 55)
		ft_put_cadre(x, y, 56, str);
	attron(COLOR_PAIR(52));
	if (ft_atoi(str) > 2048)
		mvprintw(y, x, str);
	attroff(COLOR_PAIR(52));
	attron(COLOR_PAIR(c));
	if (ft_strcmp(str, "0") != 0 && ft_atoi(str) < 2048)
		mvprintw(y, x, str);
	attroff(COLOR_PAIR(c));
}
