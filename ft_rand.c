/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rand.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 12:07:30 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/01 14:39:07 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static int	ft_rand_two_four(void)
{
	int a;

	a = (rand() < RAND_MAX * 0.9) ? 2 : 4;
	return (a);
}

static int	rand_a_b(int a, int b)
{
	return (rand() % (b - a) + a);
}

void		ft_rand_number(t_env *env)
{
	int x;
	int y;
	int flag;

	x = 0;
	y = 0;
	flag = 0;
	while (flag != 1)
	{
		x = rand_a_b(0, 4);
		y = rand_a_b(0, 4);
		if (env->squares[x][y] == 0)
		{
			env->squares[x][y] = ft_rand_two_four();
			flag = 1;
		}
	}
}
