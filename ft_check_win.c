/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_win.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 10:21:16 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/01 15:28:22 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static int	ft_result(int i, t_env *env)
{
	if (i == 1)
		env->win_0 = 1;
	else if (i == 2)
		env->win_1 = 1;
	if (i)
		return (i);
	else
		return (0);
}

int			ft_check_win(t_env *env)
{
	int x;
	int y;
	int i;

	x = 0;
	i = 0;
	while (x < 4)
	{
		y = 0;
		while (y < 4)
		{
			if ((env->squares[y][x] == WIN_1 && !env->win_0)\
					|| env->squares[y][x] == WIN_2)
				i = env->squares[y][x] == WIN_1 ? 1 : 2;
			y++;
		}
		x++;
	}
	return (ft_result(i, env));
}
