/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_error.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 00:35:32 by jpiazzal          #+#    #+#             */
/*   Updated: 2015/03/01 00:35:34 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

int		ft_error(char *str)
{
	ft_putstr("Error: ");
	ft_putendl(str);
	return (0);
}

void	initialisation_color_cases(void)
{
	init_color(10, 999, 800, 800);
	init_color(11, 999, 600, 600);
	init_color(12, 999, 400, 400);
	init_color(13, 999, 200, 200);
	init_color(14, 999, 001, 001);
	init_color(15, 800, 001, 001);
	init_color(16, 600, 001, 001);
	init_color(17, 600, 300, 001);
	init_color(18, 800, 400, 001);
	init_color(19, 999, 500, 001);
	init_color(20, 999, 999, 001);
}

void	initialisation_color(void)
{
	int		i;

	if (has_colors() == FALSE)
	{
		endwin();
		ft_error("Your terminal does not support color.");
		exit (1);
	}
	start_color();
	init_color(100, 500, 500, 500);
	init_color(200, 200, 200, 200);
	init_pair(50, COLOR_WHITE, 200);
	init_pair(51, 200, 200);
	init_pair(52, COLOR_BLACK, 100);
	init_pair(53, 100, 100);
	init_pair(54, COLOR_RED, 100);
	init_pair(55, COLOR_GREEN, 100);
	init_pair(56, COLOR_BLACK, COLOR_BLACK);
	initialisation_color_cases();
	i = 0;
	while (++i < 12)
		init_pair(i, 9 + i, 9 + i);
	i = 11;
	while (++i < 23)
		init_pair(i, COLOR_BLACK, i - 2);
}

int		check_window_size(int col, int row, int key)
{
	if (col < 49 || row < 9)
	{
		endwin();
		ft_error("window too small.");
		return (0);
	}
	if (key == 0)
		return (1);
	if (key == 27)
	{
		nodelay(stdscr, 1);
		if (getch() == ERR)
		{
			endwin();
			return (0);
		}
		nodelay(stdscr, 0);
	}
	return (1);
}

int		check_power_two(void)
{
	int		tmp;
	int		cnt;

	cnt = 2;
	tmp = 2;
	if (WIN_1 == 4)
		cnt--;
	while (tmp != WIN_1 && tmp < WIN_1 && cnt == 2)
	{
		tmp *= 2;
		if (tmp == WIN_1)
			cnt--;
	}
	tmp = 2;
	if (WIN_2 == 4)
		cnt--;
	while (tmp != WIN_2 && tmp < WIN_2 && cnt != 0)
	{
		tmp *= 2;
		if (tmp == WIN_2)
			cnt--;
	}
	return (cnt);
}
