/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/09 11:38:18 by jpiazzal          #+#    #+#             */
/*   Updated: 2014/11/09 17:16:04 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

int		ft_nb_string(char const *s, char c)
{
	int		i;
	int		count;

	i = 0;
	count = 0;
	while (s[i])
	{
		if (s[i] == c)
			i++;
		if (s[i])
		{
			count++;
			while (s[i] != c && s[i])
				i++;
		}
	}
	return (count);
}

int		ft_nb_char(char const *s, char c, int start)
{
	int		count;

	count = 0;
	while (s[start] != c && s[start])
	{
		count++;
		start++;
	}
	return (count);
}

void	ft_split_two(char **str, char const *s, char c)
{
	int		i;
	int		j;
	int		k;
	int		start;

	i = 0;
	k = 0;
	j = 0;
	while (s[i])
	{
		if (s[i] == c)
			i++;
		else
		{
			start = i;
			str[k] = (char *)ft_memalloc(ft_nb_char(s, c, start) + 1);
			if (s[i])
			{
				while (s[i] && s[i] != c)
					str[k][j++] = s[i++];
				str[k++][j] = 0;
				j = 0;
			}
		}
	}
}

char	**ft_strsplit(char const *s, char c)
{
	char	**str;

	if (!s || !c)
		return (NULL);
	if ((str = ft_memalloc(sizeof(char *) * (ft_nb_string(s, c) + 1))))
		ft_split_two(str, s, c);
	return (str);
}
