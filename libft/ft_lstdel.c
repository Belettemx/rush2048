/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/27 12:40:55 by jpiazzal          #+#    #+#             */
/*   Updated: 2014/11/27 13:33:40 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	if (!(*alst) || !(*del))
		return ;
	while ((*alst)->next)
	{
		(*del)((*alst)->content, (*alst)->content_size);
		(*alst) = (*alst)->next;
	}
	*alst = NULL;
}
