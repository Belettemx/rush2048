/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/27 14:17:11 by jpiazzal          #+#    #+#             */
/*   Updated: 2014/11/27 18:14:01 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*list;
	t_list	*alst;

	if (!lst)
		return (NULL);
	if (!(list = (t_list *)malloc(sizeof(t_list))))
		return (NULL);
	alst = NULL;
	list = alst;
	while (lst != NULL)
	{
		list = (*f)(lst);
		ft_lstadd_end(&alst, list);
		lst = lst->next;
	}
	return (alst);
}
