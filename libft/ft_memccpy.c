/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 21:24:29 by jpiazzal          #+#    #+#             */
/*   Updated: 2014/11/06 17:44:57 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	char	*t;
	char	*t2;
	size_t	i;

	i = 0;
	t = (char *)dst;
	t2 = (char *)src;
	while (i < n)
	{
		t[i] = t2[i];
		if (t2[i] == c)
			return ((void *)&t[i + 1]);
		i++;
	}
	return (NULL);
}
