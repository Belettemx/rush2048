/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/27 18:13:22 by jpiazzal          #+#    #+#             */
/*   Updated: 2014/11/27 18:13:24 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_end(t_list **alst, t_list *new)
{
	t_list	*list;

	if (!*alst)
	{
		(*alst) = new;
		return ;
	}
	list = (*alst);
	while (list->next != NULL)
		list = list->next;
	list->next = new;
}
