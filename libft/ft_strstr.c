/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 12:23:56 by jpiazzal          #+#    #+#             */
/*   Updated: 2014/11/04 13:50:44 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strstr(const char *s1, const char *s2)
{
	int		i;
	int		j;
	int		temp;

	i = 0;
	if (*s2 == '\0')
		return ((char *)&*s1);
	while (s1[i])
	{
		j = 0;
		if (s1[i] == s2[j])
		{
			temp = i;
			while (s2[j] == s1[i])
			{
				if (s2[j + 1] == '\0')
					return ((char *)&s1[temp]);
				j++;
				i++;
			}
			i = temp;
		}
		i++;
	}
	return (NULL);
}
