/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 16:11:01 by jpiazzal          #+#    #+#             */
/*   Updated: 2014/11/06 23:11:30 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int i;
	int j;
	int k;
	int nbr;

	i = ft_strlen(str);
	nbr = 0;
	j = 0;
	k = 0;
	while (str[j] == ' ' || str[j] == '\t' || str[j] == '\n' ||
		str[j] == '\v' || str[j] == '\r' || str[j] == '\f')
	{
		j++;
		k++;
	}
	while (i > j && ((str[j] >= '0' && str[j] <= '9') || (str[j] == '+' &&
		j == k) || (str[j] == '-' && j == k)))
	{
		if (str[j] != '+' && str[j] != '-')
			nbr = nbr * 10 + (str[j] - '0');
		j++;
	}
	if (str[k] == '-')
		nbr = -nbr;
	return (nbr);
}
