/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 15:58:44 by jpiazzal          #+#    #+#             */
/*   Updated: 2014/11/09 08:44:12 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_len_nb(int n)
{
	int i;

	i = 0;
	if (n < 0)
		i++;
	if (n == 0)
		return (1);
	while (n != 0)
	{
		n = n / 10;
		i++;
	}
	return (i);
}

char	*ft_itoa(int n)
{
	char	*str;
	int		len;
	long	m;

	len = ft_len_nb(n);
	m = n;
	str = (char *)malloc(sizeof(char) * (len + 1));
	str[len--] = 0;
	if (m < 0)
		str[0] = '-';
	if (m == 0)
	{
		str[0] = '0';
		return (str);
	}
	if (m < 0)
		m = m * -1;
	while (m != 0)
	{
		str[len] = (m % 10) + '0';
		m = m / 10;
		len--;
	}
	len++;
	return (str);
}
