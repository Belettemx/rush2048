/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 15:50:34 by jpiazzal          #+#    #+#             */
/*   Updated: 2014/11/06 12:24:16 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	char	*t;
	char	*t2;
	size_t	i;

	i = 0;
	t = (char *)dst;
	t2 = (char *)src;
	while (i < n)
	{
		t[i] = t2[i];
		i++;
	}
	return (dst);
}
