/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_poss.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 11:14:47 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/01 15:35:37 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static int	ft_move_poss_right(t_env *env)
{
	int i;
	int x;
	int y;

	i = 0;
	y = 0;
	while (y < 4)
	{
		x = 3;
		if (env->squares[y][x] == env->squares[y][x - 1] && env->squares[y][x])
			return (1);
		else if (env->squares[y][x - 1] == env->squares[y][x - 2]\
				&& env->squares[y][x - 1])
			return (1);
		else if (env->squares[y][x - 2] == env->squares[y][x - 3]\
				&& env->squares[y][x - 2])
			return (1);
		y++;
	}
	return (i);
}

static int	ft_move_poss_left(t_env *env)
{
	int i;
	int x;
	int y;

	i = 0;
	y = 0;
	while (y < 4)
	{
		x = 0;
		if (env->squares[y][x] == env->squares[y][x + 1] && env->squares[y][x])
			return (1);
		else if (env->squares[y][x + 1] == env->squares[y][x + 2]\
				&& env->squares[y][x + 1])
			return (1);
		else if (env->squares[y][x + 2] == env->squares[y][x + 3]\
				&& env->squares[y][x + 2])
			return (1);
		y++;
	}
	return (i);
}

static int	ft_move_poss_down(t_env *env)
{
	int i;
	int x;
	int y;

	x = 0;
	i = 0;
	while (x < 4)
	{
		y = 3;
		if (env->squares[y][x] == env->squares[y - 1][x] && env->squares[y][x])
			return (1);
		else if (env->squares[y - 1][x] == env->squares[y - 2][x]\
				&& env->squares[y - 1][x])
			return (1);
		else if (env->squares[y - 2][x] == env->squares[y - 3][x]\
				&& env->squares[y - 2][x])
			return (1);
		x++;
	}
	return (i);
}

static int	ft_move_poss_up(t_env *env)
{
	int i;
	int x;
	int y;

	i = 0;
	x = 0;
	while (x < 4)
	{
		y = 0;
		if (env->squares[y][x] == env->squares[y + 1][x] && env->squares[y][x])
			return (1);
		else if (env->squares[y + 1][x] == env->squares[y + 2][x]\
				&& env->squares[y + 1][x])
			return (1);
		else if (env->squares[y + 2][x] == env->squares[y + 3][x]\
				&& env->squares[y + 2][x])
			return (1);
		x++;
	}
	return (i);
}

int			ft_move_poss(t_env *env)
{
	int i;

	i = 0;
	if ((i = ft_move_poss_right(env)))
		return (i);
	if ((i = ft_move_poss_left(env)))
		return (i);
	if ((i = ft_move_poss_down(env)))
		return (i);
	if ((i = ft_move_poss_up(env)))
		return (i);
	return (i);
}
