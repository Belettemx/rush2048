/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algorithme.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 15:49:46 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/01 14:14:03 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static int	ft_check_wl(t_env *env)
{
	int i;

	i = ft_check_win(env);
	if (i == 1 || i == 2)
		return (i);
	if (!ft_check_lost(env))
		return (-1);
	return (0);
}

static void	ft_squares_init(t_env *env)
{
	int x;
	int y;

	y = -1;
	while (++y < 4)
	{
		x = -1;
		while (++x < 4)
			env->squares[y][x] = 0;
	}
	ft_rand_number(env);
	ft_rand_number(env);
	env->win_0 = 0;
	env->win_1 = 0;
	env->lost = 0;
}

int			ft_algorithme(int key, t_env *env)
{
	if (key == INIT)
		ft_squares_init(env);
	else if (key == KEY_UP)
	{
		if (ft_up(env) == 1)
			ft_rand_number(env);
	}
	else if (key == KEY_DOWN)
	{
		if (ft_down(env) == 1)
			ft_rand_number(env);
	}
	else if (key == KEY_LEFT)
	{
		if (ft_left(env) == 1)
			ft_rand_number(env);
	}
	else if (key == KEY_RIGHT)
	{
		if (ft_right(env) == 1)
			ft_rand_number(env);
	}
	return (ft_check_wl(env));
}
