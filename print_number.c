/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_number.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 21:12:25 by jpiazzal          #+#    #+#             */
/*   Updated: 2015/03/01 21:12:26 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

void	print_number(int row, int col, t_env *env)
{
	int		add_col;
	int		add_row;
	int		tx;
	int		ty;
	char	*str;

	add_col = (col - (col / 4 * 3)) / 2;
	add_row = (row - (row / 4 * 3)) / 2;
	ty = -1;
	while (++ty < 4)
	{
		tx = -1;
		while (++tx < 4)
		{
			if (env->y == (row / 4 * ty) + add_row &&
				(env->x == (col / 4 * tx) + add_col))
			{
				str = ft_itoa(env->squares[ty][tx]);
				put_color(env->x - 2, env->y,
					check_number_color(env->squares[ty][tx]) + 11, str);
				free(str);
			}
		}
	}
}
