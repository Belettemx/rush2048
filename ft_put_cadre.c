/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_cadre.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 21:12:17 by jpiazzal          #+#    #+#             */
/*   Updated: 2015/03/01 21:12:18 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static void	print_line_cadre(int ty, int x, int add, int c)
{
	attron(COLOR_PAIR(c));
	mvprintw(ty, x - add, " ");
	attron(COLOR_PAIR(c));
	attron(COLOR_PAIR(c));
	mvprintw(ty, x + add * 3, " ");
	attron(COLOR_PAIR(c));
}

void		ft_put_cadre(int x, int y, int c, char *str)
{
	int		tx;
	int		ty;
	int		add;

	ty = y - 2;
	add = (int)ft_strlen(str) / 2;
	while (ty <= y + 2)
	{
		tx = x - add;
		while (tx <= x + add * 3)
		{
			attron(COLOR_PAIR(53));
			mvprintw(ty, tx, " ");
			attron(COLOR_PAIR(53));
			if (ty == y - 2 || ty == y + 2)
			{
				attron(COLOR_PAIR(c));
				mvprintw(ty, tx, " ");
				attron(COLOR_PAIR(c));
			}
			tx++;
		}
		print_line_cadre(ty, x, add, c);
		ty++;
	}
}
