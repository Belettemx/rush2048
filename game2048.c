/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game2048.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpiazzal <jpiazzal@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 00:48:30 by jpiazzal          #+#    #+#             */
/*   Updated: 2015/03/01 21:34:38 by jpiazzal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

void	ft_print_interface(int row, int col, int check)
{
	while ((row - 1) % 4 != 0)
		row--;
	while ((col - 1) % 4 != 0)
		col--;
	if (check == -1)
		put_color(col / 2 - 4, row / 2, 54, "GAME OVER");
	if (check == 1)
		put_color(col / 2 - 3, row / 2, 55, "YOU WIN");
	if (check == 2)
		put_color(col / 2 - 13, row / 2, 55, "YOU WIN : YOU REACH THE MAX");
}

int		print_game_loop(int row, int col, int key, t_env *env)
{
	int		check;

	clear();
	getmaxyx(stdscr, row, col);
	if (check_window_size(col, row, key) == 0)
		return (0);
	check = ft_algorithme(key, env);
	draw_cases(row, col, env);
	refresh();
	ft_print_interface(row, col, check);
	refresh();
	return (1);
}

void	print_game(void)
{
	int		key;
	int		row;
	int		col;
	int		check;
	t_env	env;

	initscr();
	curs_set(0);
	raw();
	cbreak();
	keypad(stdscr, TRUE);
	noecho();
	initialisation_color();
	getmaxyx(stdscr, row, col);
	if (check_window_size(col, row, 0) == 0)
		return ;
	check = ft_algorithme(INIT, &env);
	draw_cases(row, col, &env);
	ft_print_interface(row, col, check);
	refresh();
	while ((key = getch()))
		if (print_game_loop(row, col, key, &env) == 0)
			return ;
}

int		main(int argc, char **argv)
{
	srand(time(NULL));
	if (check_power_two() != 0)
		return (ft_error("WIN_VALUE is not correct."));
	if (argc == 1)
		print_game();
	else
		ft_putendl("Please use only one argument.");
	(void)argv;
	return (0);
}
